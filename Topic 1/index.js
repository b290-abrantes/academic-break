// console.log("Topic 1")

function shipItem(item, weight, location){
	let shippingFee;
	if (weight >= 1 || weight === 3){
		shippingFee = 150;
	}
	if (weight > 3 || weight === 5){
		shippingFee = 280;
	}
	if (weight > 5 || weight === 8){
		shippingFee = 340;
	}
	if (weight > 8 || weight === 10){
		shippingFee = 410;
	}
	if (weight > 10){
		shippingFee = 560;
	};
	let charge;
	switch (location){
		case "local":
			charge = 0;
			break;
		case "international":
			charge = 250

	};
	let totalShippingFee = shippingFee + charge;
	return "The total shipping fee for a " + item + " that will ship " + location + " is " + totalShippingFee;
};
shipItem();