function Product(name, price, quantity){
	this.name = name;
	this.price = price;
	this.quantity = quantity;
};

function Customer(name, cart, addToCart, removeToCart){
	this.name = name;
	this.cart = [];
	this.addToCart = function(product){
			this.cart.push(product);
			return `${product.name} is added to cart.`;
	};
	this.removeToCart = function(product){
			this.cart.pop(product);
			return `${product.name} is removed from the cart.`;
	};
};
let cust = new Customer("Rusty");
let prod = new Product("Tote bag", 150, 50);

console.log(cust.addToCart(prod));
console.log(cust);
console.log(cust.removeToCart(prod));
console.log(cust);

